nNum1 = 1
nNum2 = 1
nDigit = int(input("Введите число: "))

for i in range(2, nDigit):
    nNum1, nNum2 = nNum2, nNum1 + nNum2

print(f"{nDigit} член из последовательности Фибоначчи: {nNum2}")
